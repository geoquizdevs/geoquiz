package com.geoquiz.model.xmlunmarshal;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class implements the concept of info on a country. It is used to unmarshal the CountriesInfo XML file.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "country",
    "capitalCity",
    "currency",
    "flagRef",
    "difficulty"
})
@XmlRootElement(name = "CountryInfo")
public class CountryInfo {

    @XmlElement(name = "Country", required = true)
    private String country;
    @XmlElement(name = "CapitalCity", required = true)
    private String capitalCity;
    @XmlElement(name = "Currency", required = true)
    private String currency;
    @XmlElement(name = "FlagRef", required = true)
    private String flagRef;
    @XmlElement(name = "Difficulty", required = true)
    private String difficulty;

    /**
     * Gets the value of country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCountry(final String value) {
        this.country = value;
    }

    /**
     * Gets the value of capitalCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCapitalCity() {
        return capitalCity;
    }

    /**
     * Sets the value of capitalCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCapitalCity(final String value) {
        this.capitalCity = value;
    }

    /**
     * Gets the value of currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCurrency(final String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the flagRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFlagRef() {
        return flagRef;
    }

    /**
     * Sets the value of the flagRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFlagRef(final String value) {
        this.flagRef = value;
    }

    /**
     * Gets the value of difficulty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDifficulty() {
        return difficulty;
    }

    /**
     * Sets the value of difficulty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDifficulty(final String value) {
        this.difficulty = value;
    }

}
