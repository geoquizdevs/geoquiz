package com.geoquiz.model.xmlunmarshal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * This class implements the concept of info on a monument. It is used to unmarshal the Monuments{Easy, Medium, Difficult} XML file
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "monument",
    "country"
})
@XmlRootElement(name = "MonumentInfo")
public class MonumentInfo {

    @XmlElement(name = "Monument", required = true)
    private String monument;
    @XmlElement(name = "Country", required = true)
    private String country;

    /**
     * Gets the value of the monument property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMonument() {
        return monument;
    }

    /**
     * Sets the value of the monument property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMonument(final String value) {
        this.monument = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCountry(final String value) {
        this.country = value;
    }

}
