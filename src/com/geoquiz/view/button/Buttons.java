package com.geoquiz.view.button;

/**
 * The menu's buttons who a player can press.
 */
public enum Buttons {
    /**
     * Log in if user is authenticted.
     */
    ACCEDI,
    /**
     * Possibility to create an account.
     */
    REGISTRATI,
    /**
     * Exit the game.
     */
    ESCI,
    /**
     * Go to scene before.
     */
    INDIETRO,
    /**
     * Enter in category scene.
     */
    GIOCA,
    /**
     * Enter in option scene.
     */
    OPZIONI,
    /**
     * Register a new accont.
     */
    SALVA,
    /**
     * Enter in ranking scene.
     */
    CLASSIFICA,
    /**
     * Enter in statistics scene.
     */
    STATISTICHE,

    /**
     * Enter in instructions scene.
     */
    ISTRUZIONI;

}
