package com.geoquiz.view.label;

/**
 * A label inside the menu.
 */
public interface MyLabel {
    /**
     * set the text of label.
     * 
     * @param text
     *            the text of label.
     */
    void setText(String text);

    /**
     * Gets the text of the label.
     * 
     * @return the text o the label.
     */
    String getText();
}
